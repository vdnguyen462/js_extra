/* ----------Bài 1 ---------- */
function inBang(){
    document.write("<table>")
    for (var i=1; i < 100; i+=10){
        document.write("<tr>");
        for(var j=i; j<i+10; j ++){
            document.write("<td>"+j+"</td>");
        }
        document.write("</tr>")
    }
    document.write("</table>");
}



/* ----------Bài 2 ---------- */
var arr = [];
var arr2 = [];
let nhapSo = () => {
    var n = document.getElementById("txt-nhap-so").value*1;
    arr.push(n);
    document.getElementById("txt-them-so").setAttribute('value',`Giá trị mảng: ${arr}`);
    document.getElementById("txt-nhap-so").value = "";
}

let kiemTraNguyenTo = num => {
    if(num < 2)
        return false;
    for(let i = 2; i <= Math.sqrt(num); i++) {
        if(num % i == 0) 
            return false;
    }
    return true;
}

let timSoNguyenTo = () => {
    for (var i=0; i<arr.length; i++){
        if(kiemTraNguyenTo(arr[i])){
            arr2.push(arr[i]);
            document.getElementById("txt-timNguyenTo").setAttribute('value',`Giá trị mảng: ${arr2}`);
        }
    }
    arr2 = [];
}

/* ----------Bài 3 ---------- */

let tinhTong = () => {
    var n = document.getElementById("txt-nhap-n").value*1;
    var s=0, s1=0, s2=0;
    for(var i=2; i <= n; i++){
        s1 = s1 + i;
        s2 = 2*n;
        s = s1+s2;
    }
    document.getElementById("txt-nhap-n").value = "";
    document.getElementById("txt-tinhTong").setAttribute('value',`Tổng: ${s}`);
}

/* ----------Bài 4 ---------- */

let timUoc = () => {
    var n = document.getElementById("txt-nhap-so-n").value*1;
    var arr = [];
    for(var i=0; i <= n; i++){
        if(n%i==0)
            arr.push(i);
    }
    document.getElementById("txt-nhap-so-n").value = "";
    document.getElementById("txt-timUoc").setAttribute('value',`${arr}`);
}

/* ----------Bài 5---------- */
let timSoDao = () => {
    var n = document.getElementById("txt-nhap-so-nguyen-duong").value*1;
    var res=0;
    while (n){
        res = (res*10)+n%10;
        n = n/10|0;
    }
    document.getElementById("txt-nhap-so-nguyen-duong").value = "";
    document.getElementById("txt-timSoDao").setAttribute('value',`${res}`);
}

/* ----------Bài 6---------- */
let timNguyenDuong = () => {
    var sum=0;
    var i;
    for(i=0; i<100; i++){
        sum+=i;
        if(sum >= 100)
            break;
    }
    document.getElementById("txt-tim-nguyen-duong").setAttribute('value',`${i-1}`);
}

/* ----------Bài 7---------- */
let inBangCuuChuong = () => {
    var n = document.getElementById("txt-so-n").value*1;
    let result;
    let contentHTML = "";
    for(i=1; i<=10; i++){
        result = n*i;
        var content = `${n} x ${i} = ${result}<br>`
        contentHTML+=content;
    }
    document.getElementById("txt-bang-cuu-chuong").innerHTML = contentHTML;
}

/* ----------Bài 8---------- */
let chiaBai = () => {
    var players = [[], [], [], []];
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
    let contentHTML = "";
    for(let i=0; i<players.length; i++){
        for(let j=i; j< cards.length; j=j+4){
            players[i].push(cards[j]);
        }
        contentHTML+=`var player${i+1} = ${players[i]} <br>`;
    }
    document.getElementById("txt-show").innerHTML = contentHTML;
}

/* ----------Bài 9---------- */
let timGaVaCho = () => {
    var m = document.getElementById("txt-tong-ga-cho").value*1;
    var n = document.getElementById("txt-tong-chan").value*1;
    for(let i =1; i<m; i++){
        for(let j=1; j<m; j++){
            if((i*2+j*4)==n && i+j == m){
                document.getElementById("txt-tim-ga-cho").setAttribute('value',`Gà: ${i} con, chó: ${j} con`);
            }
        }
    }
}

/* ----------Bài 10---------- */
let timGocLech = () => {
    let hour = document.getElementById("txt-gio").value*1;
    let min = document.getElementById("txt-phut").value*1;
    let result = Math.abs(min*6-0.5*(hour*60+min))
    document.getElementById("txt-tim-goc-lech").setAttribute('value',`Góc lệch: ${result} độ`);
}




function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}


function bai3(){
    document.getElementById("e3").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai4(){
    document.getElementById("e4").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai5(){
    document.getElementById("e5").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai6(){
    document.getElementById("e6").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai7(){
    document.getElementById("e7").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai8(){
    document.getElementById("e8").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e9").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai9(){
    document.getElementById("e9").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e10").style.display = "none";
}

function bai10(){
    document.getElementById("e10").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
    document.getElementById("e5").style.display = "none";
    document.getElementById("e6").style.display = "none";
    document.getElementById("e7").style.display = "none";
    document.getElementById("e8").style.display = "none";
    document.getElementById("e9").style.display = "none";
}